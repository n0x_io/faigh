# Faigh:
---
Faigh is a system fetcher tool like neofetch. Written in C++. Faigh can only really be used on UNIX based system though was developed for Linux. So I can't confirm how well it works on BSD or Mac OS X. As I'm not familiar enough with differences in those systems.

This is my first public project I'd gladly accpet any help in realtion to improving my code. As I'm hoping I can learn a lot from people who know more than me and know how to improve things in ways I do not. Any bugs I'll also be happy to fix if you file an issue here.

This program is currently not fully finished and does not reflect a finished product. However you can submit issues to me and I'll be happy to fix them.

---
## How to use it:

### Example output: 
```
$ Faigh ./Faigh 
Kernel Name: Linux x86_64
Host Name: CatArch
Kernel Version: 5.11.1-arch1-1
Operating System: "Arch Linux"
CPU Model: Ryzen 5 3600X 6-Core Processor
CPU Cores: 6 Cores/12 Threads 
CPU Speed: 4408593KHz Max
Ram Usage: 9425MiB/16018MiB
Sys Uptime: 3 Hours, 7 Mins, 14 Seconds.

```

---
### How to build it:
--- 
To build this tool you should just have to run `g++ -o Faigh main.cpp` or `make` should also work. However if you are not using gcc compilers then you will need to substitute this command for your compilers syntax.

```
g++ -o Faigh main.cpp

or

make
```

---
### How to run it:
---
```
./Faigh
```

currently cmdline arguments aren't allowed but I do plan to implement them.

---
## To do:
---
- [X] Write Readme
- [X] Get CPU model, speed and cores
- [X] Get RAM size and used RAM
- [X] Display uptime in a readable format
- [X] Display Hostname, Kernel Name & Version
- [] Get GPUs and their details
- [] Get WM/DE in use
- [] Display terminal being used
- [] Themes being used
- [] Add cmdline arguments
- [] Resolution of monitor
- [] Add artwork and make it look nice
- [] Licesnse the code
