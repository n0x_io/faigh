#include <iostream>
#include <fstream>
#include <string>
#include <sys/utsname.h>
#include <sys/sysinfo.h>

/*
 * @Author: CaitCat
 * Description: Basic System information fetcher written in C++
 * Date: 25/02/2021
 * License: 
 */


struct utsname unameData;

/*
 * searchFor used if we want to search for a string on a certain line. 
 * strFilter is used to move the pointer along to only return what we want.
 * fileName is the name of the file we input     
 */
std::string fileInfoGet(std::string fileName, std::string searchFor, int strFilter)
{
    std::string output; //initialize output left empty for now
    std::ifstream inFileStr(fileName, std::ios::in); 
    if(!inFileStr.is_open()){ // if we can't open the file put that into output and print that instead
	output = "Unable to open fileName: " + fileName;
	return output;
    } else { //Else we start to search for our line/data
        while(getline(inFileStr, output)) // Puts the value of the current line into the output variable
	{	
            if(output.find(searchFor) != std::string::npos) //looks for what we want in the string and runs if found
	    {	
	        output = output.substr(strFilter ,std::string::npos); //Filter the string to what we want and nothing else
		return output;//if we find what we are looking for then we print that
	    }
	}
    }
    return "File Open but string not found";
}


// Takes in the uptime in seconds and then coverts them into a readable format
std::string refineUptime(int uptimeInSeconds){
    // Static integers for conversion to avoid "magic numbers"
    static int secondsPerMinute = 60;
    static int secondsPerHour = 60*secondsPerMinute;
    static int secondsPerDay = 24*secondsPerHour;
    // output -> return of function
    std::string output = ""; 

    // Calculate Days and add to output
    if( uptimeInSeconds / secondsPerDay >= 1){
        output = output + std::to_string(uptimeInSeconds/secondsPerDay) + " Days, ";
    }
    // Calculate Hours and add to output
    if( (uptimeInSeconds % secondsPerDay) / secondsPerHour >= 1){
        output = output + std::to_string((uptimeInSeconds % secondsPerDay) / secondsPerHour ) + " Hours, ";
    }
    // Calculate Minutes and add to output
    if( (uptimeInSeconds % secondsPerHour /secondsPerMinute) >= 1){
        output = output + std::to_string((uptimeInSeconds % secondsPerHour) / secondsPerMinute) + " Mins, ";
    }
    // Calculate Seconds and add to output
    if( (uptimeInSeconds % secondsPerMinute) >= 1){
        output = output + std::to_string(uptimeInSeconds % secondsPerMinute) + " Seconds. ";
    }
 
    //send output to be printed out.
    return output;
}



int main(void){ //currently CMD argues not allowed and should not be taken.
	
    struct sysinfo info; //defines sysinfo struct as info as the name
    sysinfo(&info); // Moves data from Sysinfo into info structure to be used here. (used for uptime and RAM)
    uname(&unameData); //Moves the uname data into the unameData variable for printing.
    
    int totalRam = info.totalram / 1024 / 1024; //converts to MiB
    int freeRam =  info.freeram / 1024 / 1024; //This could be a problem on higher end rigs but I plan to implement a better method for this later Should be removed after testing

    //bit of a mess but suffices to say prints and calls the functions to grab the system data.
    std::cout << "Kernel Name: " << unameData.sysname << " " << unameData.machine << std::endl;
    std::cout << "Host Name: " << unameData.nodename << std::endl;
    std::cout << "Kernel Version: " <<  unameData.release << std::endl;
    std::cout << "Operating Sys: " << fileInfoGet("/etc/os-release", "NAME=", 5) << std::endl;
    std::cout << "CPU Model: " << fileInfoGet("/proc/cpuinfo", "model name", 17) << std::endl;
    std::cout << "CPU Cores: " << fileInfoGet("/proc/cpuinfo", "cpu cores", 12) << " Cores/" << fileInfoGet("/proc/cpuinfo", "siblings", 11) << " Threads " << std::endl;
    std::cout << "CPU Speed: " << fileInfoGet("/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq", "", 0) << "KHz Max" << std::endl;
    std::cout << "Ram Usage: " << totalRam - freeRam << "MiB/" << totalRam << "MiB" << std::endl;
    std::cout << "Sys Uptime: " << refineUptime(info.uptime) << std::endl;
}
